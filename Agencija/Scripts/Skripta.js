﻿$(window).on(function () {
    $("#nekretnine").click();
});

$(document).ready(function () {
    var host = window.location.host;
    var token = null;
    var headers = {};
    var formAction = "Create";
    var editingId;

    $("#odjava").css("display", "none");
    $("#regIprij").click(function () {
        $("#pocetak").css("display", "block");
        $("#regIprij").css("display", "none");
        $("#prijavaDiv").css("display", "block");
    });

    $("#btnRegistracija").click(function () {

        $("#registracijaDiv").css("display", "block");
        $("#btnRegistracija").css("display", "none");
    });

    $("#registracija").submit(function (e) {
        e.preventDefault();

        var email = $("#regEmail").val();
        var lozinka1 = $("#regLozinka").val();
        var lozinka2 = $("#regLozinka2").val();

        var objekatZaSlanje = {
            "Email": email,
            "Password": lozinka1,
            "ConfirmPassword": lozinka2
        };

        $.ajax({
            type: "POST",
            url: 'http://' + host + "/api/Account/Register",
            data: objekatZaSlanje

        })
            .done(function (data) {
                clearForm();
                $("#info").append("Uspešna registracija. Možete se prijaviti na sistem!");
            })
            .fail(function (data) {
                alert("Greška prilikom registracije");
            });
    });

    $("#prijava").submit(function (e) {
        e.preventDefault();

        var email = $("#priEmail").val();
        var lozinka = $("#priLozinka").val();

        var objekatZaSlanje = {
            "grant_type": "password",
            "username": email,
            "password": lozinka
        };

        $.ajax({
            "type": "POST",
            "url": 'http://' + host + "/Token",
            "data": objekatZaSlanje
        })
            .done(function (data) {
                console.log(data);
                $("#info").empty().append("Prijavljen korisnik: " + data.userName);
                token = data.access_token;
                $("#prijava").css("display", "none");
                $("#registracija").css("display", "none");
                $("#odjava").css("display", "block");
                $("#nekretnine").trigger("click");
                $("#infoKorisnik").css("display", "none");
                $("#formDiv").css("display", "block");
                $("#filterDiv").css("display", "block");
            })
            .fail(function (data) {
                alert("Greška prilikom prijave!");
            });
    });

    $("#odjava").click(function () {
        token = null;
        headers = {};

        $("#info").empty();
        $("#data").empty();
        $("#nekretnine").trigger("click");
        $("#regIprij").css("display", "block");
        $("#odjava").css("display", "none");
        $("#formDiv").css("display", "none");
        $("#filterDiv").css("display", "none");
        $("#infoKorisnik").css("display", "block");
    });

    $("body").on("click", "#btnDelete", deleteNekretnina);
    $("body").on("click", "#btnEdit", editNekretnina);

    $("#nekretnine").click(function () {

        if (token) {
            headers.Authorization = 'Bearer' + token;
        }

        $.ajax({
            "type": "GET",
            "url": "http://" + host + "/api/nekretnine",
            "headers": headers
        })
            .done(function (data, status) {
                //clearForm();
                var $container = $("#data");
                $container.empty();

                if (status === "success") {
                    console.log(data);
                    if (token) {
                        var div = $("<div></div>");
                        var h1 = $("<center><h1>Nekretnine</h1></center>");
                        div.append(h1);

                        var table = $("<table class='table table-bordered'></table><br/>");
                        var header = $("<thead class='thead'><tr><th>Mesto</th><th>Oznaka</th><th>Izgradnja</th><th>Kvadratura</th><th>Cena</th><th>Agent</th><th>Obriši</th><th>Izmeni</th></tr></thead>");
                        table.append(header);

                        for (i = 0; i < data.length; i++) {
                            var row = "<tr>";
                            var display = "<td>" + data[i].Mesto + "</td><td>" + data[i].Oznaka + "</td><td>" + data[i].Godina_izgradnje + "</td><td>" + data[i].Kvadratura + "</td><td>" + data[i].Cena + "</td><td>" + data[i].Agent.Ime_Prezime + "</td>";
                            var stringId = data[i].Id.toString();
                            var displayDelete = "<td><button id=btnDelete name=" + stringId + ">Delete</button></td>";
                            var displayEdit = "<td><button id=btnEdit name=" + stringId + ">Edit</button></td>";
                            row += display + displayDelete + displayEdit + "</tr>";
                            table.append(row);
                            newId = data[i].Id;

                            console.log(data);

                        }
                        div.append(table);
                        $container.append(div);
                    }
                    else {
                        div = $("<div></div>");
                        h1 = $("<center><h1>Nekretnine</h1></center>");
                        div.append(h1);

                        table = $("<table class='table table-bordered'></table><br/>");
                        header = $("<thead class='thead'><tr><th>Mesto</th><th>Oznaka</th><th>Izgradnja</th><th>Kvadratura</th><th>Cena</th><th>Agent</th></tr></thead>");
                        table.append(header);

                        for (i = 0; i < data.length; i++) {
                            row = "<tr>";
                            display = "<td>" + data[i].Mesto + "</td><td>" + data[i].Oznaka + "</td><td>" + data[i].Godina_izgradnje + "</td><td>" + data[i].Kvadratura + "</td><td>" + data[i].Cena + "</td><td>" + data[i].Agent.Ime_Prezime + "</td>";
                            stringId = data[i].Id.toString();

                            row += display + "</tr>";
                            table.append(row);
                            newId = data[i].Id;

                            console.log(data);

                        }
                        div.append(table);
                        $container.append(table);
                    }
                }
                else {
                    div = $("<div></div>");
                    h1 = $("<h1>Greška prilikom preuzimanja nekretnina!</h1>");
                    div.append(h1);
                    $container.append(div);
                }
            })
            .fail(function (data) {
                alert("Greška!");
            });

    });

    function editNekretnina() {
        var editId = this.name;

        $.ajax({
            "type": "GET",
            "url": "http://" + host + "/api/nekretnine/" + editId.toString(),
            "headers": headers
        })
            .done(function (data, status) {

                $("#agentId").val(data.AgentId);
                $("#oznaka").val(data.Oznaka);
                $("#mesto").val(data.Mesto);
                $("#godIzgradnje").val(data.Godina_izgradnje);
                $("#kvadratura").val(data.Kvadratura);
                $("#cena").val(data.Cena);
                editingId = data.Id;
                formAction = "Update";
            })
            .fail(function (data, status) {
                formAction = "Create";
                alert("Desila se greška");
            });
    };
    $("#nekretnineForm").submit(function (e) {
        e.preventDefault();

        var nekretnineAgent = $("#agentId").val();
        var nekretnineOznaka = $("#oznaka").val();
        var nekretnineMesto = $("#mesto").val();
        var nekretnineIzgradnja = $("#godIzgradnje").val();
        var nekretnineKvadratura = $("#kvadratura").val();
        var nekretnineCena = $("#cena").val();
        var httpAction;
        var objekatZaSlanje;
        var url;

        httpAction = "PUT",
            url = "http://" + host + "/api/nekretnine/" + editingId.toString();
        objekatZaSlanje = {
            "Id": editingId,
            "AgentId": nekretnineAgent,
            "Oznaka": nekretnineOznaka,
            "Mesto": nekretnineMesto,
            "Godina_izgradnje": nekretnineIzgradnja,
            "Kvadratura": nekretnineKvadratura,
            "Cena": nekretnineCena
            
        };
        console.log("Objekat za slanje");
        console.log(objekatZaSlanje);
        $.ajax({
            url: url,
            type: httpAction,
            "headers": headers,
            data: objekatZaSlanje
        })
            .done(function (data, status) {
                formAction = "Create";
                refreshTable();
            })
            .fail(function (data, status) {
                alert("Desila se greška");
            });
    });
    $("#filterForm").submit(function (e) {

        e.preventDefault();

        var kvadraturaOd = $("#od").val();
        var kvadraturaDo = $("#do").val();
        var httpAction;
        var objekatZaSlanje;
        var url;

        httpAction = "POST",
            url = "http://" + host + "/api/pretraga/";
        objekatZaSlanje = {
            "mini": kvadraturaOd,
            "maksi": kvadraturaDo
        };
        console.log("Objekat za slanje");
        console.log(objekatZaSlanje);

        $.ajax({
            url: url,
            type: httpAction,
            "headers": headers,
            data: objekatZaSlanje
        })
            .done(function (data, status) {
                refreshTable();
            })
            .fail(function (data, status) {
                alert("Desila se greška");
            });
    });

    function refreshTable() {

        $("#agentId").val('');
        $("#oznaka").val('');
        $("#mesto").val('');
        $("#godIzgradnje").val('');
        $("#kvadratura").val('');
        $("#cena").val('');

        $("#nekretnine").trigger("click");
    };

    function clearForm() {

        $("#agentId").val('');
        $("#oznaka").val('');
        $("#mesto").val('');
        $("#godIzgradnje").val('');
        $("#kvadratura").val('');
        $("#cena").val('');

        $("#regEmail").val('').empty();
        $("#regLozinka").val('').empty();
        $("#regLozinka2").val('').empty();
    };

    function deleteNekretnina() {

        var deleteId = this.name;

        $.ajax({
            "type": "DELETE",
            "url": "http://" + host + "/api/nekretnine/" + deleteId.toString(),
            "headers": headers
        })
            .done(function () {
                refreshTable();
            })
            .fail(function () {
                alert("Desila se greška");
            });
    };

});