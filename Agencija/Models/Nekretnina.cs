﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Web;

namespace Agencija.Models
{
    public class Nekretnina
    {
        [Key]
        public int Id { get; set; }
        [Required]
        [StringLength(40)]
        public string Mesto { get; set; }
        [Required]
        [StringLength(5)]
        public string Oznaka { get; set; }
        [Range(1899, 2019)]
        public int Godina_izgradnje { get; set; }
        [Required]
        [Range(2, double.PositiveInfinity)]
        public decimal Kvadratura { get; set; }
        [Required]
        [Range(0, 100001.00)]
        public decimal Cena { get; set; }
        [ForeignKey("Agent")]
        public int AgentId { get; set; }
        public Agent Agent { get; set; }
    }
}