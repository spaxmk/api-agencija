﻿using Agencija.Interfaces;
using Agencija.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Web.Http;
using System.Web.Http.Description;

namespace Agencija.Controllers
{
    public class NekretnineController : ApiController
    {
        private INekretninaRepository _repository { get; set; }
        public NekretnineController(INekretninaRepository repository)
        {
            _repository = repository;
        }
        //GET api/nekretnine
        [ResponseType(typeof(Nekretnina))]
        public IEnumerable<Nekretnina> GetNekretnine()
        {
            return _repository.GetAll();
        }
        //GET api/nekretnine/1
        [ResponseType(typeof(Nekretnina))]
        public IHttpActionResult GetNekretnina(int id)
        {
            var nekretnina = _repository.GetById(id);
            if(nekretnina == null)
            {
                return NotFound();
            }
            return Ok(nekretnina);
        }
        //GET api/nekretnine
        [ResponseType(typeof(Nekretnina))]
        public IEnumerable<Nekretnina> GetGodinaIzgradnje(int napravljeno)
        {
            return _repository.GetNapravljeno(napravljeno);
        }
        //POST api/nekretnine
        [ResponseType(typeof(Nekretnina))]
        public IHttpActionResult PostNekretnina(Nekretnina nekretnina)
        {
            if (!ModelState.IsValid)
            {
                return BadRequest(ModelState);
            }
            _repository.Add(nekretnina);
            return CreatedAtRoute("DefaultApi", new { id = nekretnina.Id }, nekretnina);
        }
        //PUT api/nekretnine/1
        [ResponseType(typeof(Nekretnina))]
        public IHttpActionResult PutNekretnina(int id, Nekretnina nekretnina)
        {
            if (!ModelState.IsValid)
            {
                return BadRequest(ModelState);
            }
            if(id != nekretnina.Id)
            {
                return BadRequest();
            }
            try
            {
                _repository.Update(nekretnina);
            }
            catch
            {
                return BadRequest();
            }
            return Ok(nekretnina);
        }
        //DELETE api/nekretnine/1
        [ResponseType(typeof(Nekretnina))]
        public IHttpActionResult DeleteNekretnina(int id)
        {
            var nekretnina = _repository.GetById(id);
            if(nekretnina == null)
            {
                return NotFound();
            }
            _repository.Delete(nekretnina);
            return Ok();
        }
        //POST api/pretraga
        [ResponseType(typeof(Nekretnina))]
        [Route("api/pretraga")]
        public IEnumerable<Nekretnina> PostPretraga(int mini, int maksi)
        {
            return _repository.Pretraga(mini, maksi);
        }
    }
}
